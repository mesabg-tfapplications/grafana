
# Grafana Application Module

This module is capable to deploy grafana on an existing ECS cluster and connected it to a CDN domain or CloudMap public namespace

Module Input Variables
----------------------

- `environment` - environment name
- `project_name` - general project name
- `cluster_name` - cluster name
- `subnet_ids` - subnets identifiers
- `security_group` - security group to be applied on the application
- `log_group` - to store the logs
- `namespace_id` - cloud map namespace identifier
- `namespace_name` - cloud map namespace domain name
- `cpu` - cpu units to allocate (default 256)
- `memory` - memory to allocate (default 512)

Usage
-----

```hcl
module "grafana" {
  source          = "git::https://gitlab.com/mesabg-tfapplications/grafana.git"

  environment     = "environment"
  project_name    = "name"
  cluster_name    = "cluster"

  subnet_ids      = ["subnet-xxxx"]
  security_group  = "sg-xxxx"
  log_group       = "/some/group"

  namespace_id    = "ns-xxxx"
  namespace_name  = "default.domain"
}
```

Outputs
=======

 - `dns_name` - Domain where the application is hosted


Authors
=======
##### Moisés Berenguer <moises.berenguer@gmail.com>
