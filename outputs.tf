output "dns_name" {
  value       = "grafana.${var.namespace_name}"
  description = "Resource DNS name"
}
