terraform {
  required_version = ">= 0.13.2"
}

locals {
  application_name  = "grafana"
}

module "volume" {
  source            = "git::https://gitlab.com/mesabg-tfmodules/file_system.git?ref=v1.0.1"

  name              = "${var.project_name}_${local.application_name}_${var.environment}"
  environment       = var.environment
  persistent        = true

  subnet_ids        = var.subnet_ids
  security_groups   = [var.security_group]
}

data "aws_ecs_cluster" "ecs_cluster" {
  cluster_name = var.cluster_name
}

resource "aws_service_discovery_service" "discovery_service" {
  name = local.application_name

  dns_config {
    namespace_id = var.namespace_id

    dns_records {
      ttl  = 60
      type = "A"
    }

    dns_records {
      ttl  = 60
      type = "AAAA"
    }

    routing_policy = "WEIGHTED"
  }

  health_check_config {
    failure_threshold = 1
    resource_path     = "/"
    type              = "HTTP"
  }
}

resource "aws_cloudformation_stack" "grafana" {
  depends_on                      = [module.volume]
  name                            = "${var.project_name}-${local.application_name}-${var.environment}"

  parameters    = {
    ProjectName                   = "${var.project_name}-${var.environment}"
    ClusterArn                    = data.aws_ecs_cluster.ecs_cluster.arn
    LogGroupName                  = var.log_group
    Subnets                       = join(",", var.subnet_ids)
    CloudMapNamespaceName         = var.namespace_name
    SecurityGroup                 = var.security_group
    TaskExecutionRoleARN          = var.task_execution_role_arn
    VolumeFileSystemId            = module.volume.file_system_id
    VolumeFileSystemAccessPointId = module.volume.access_point_id
    Cpu                           = tostring(var.cpu)
    Memory                        = tostring(var.memory)
  }

  capabilities  = ["CAPABILITY_IAM", "CAPABILITY_NAMED_IAM", "CAPABILITY_AUTO_EXPAND"]
  on_failure    = "DELETE"

  template_body = file("${path.module}/stack.json")
}
